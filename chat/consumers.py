from django.contrib.auth.models import User
from asgiref.sync import async_to_sync
from channels.generic.websocket import WebsocketConsumer
import json
from django.core.files import File
from .models import Message, Room

import io
from base64 import b64decode
from django.core.files.base import File


class ChatConsumer(WebsocketConsumer):
    """
    Consumer to send and receive request on the websocket
    """


    def fetch_messages(self, data):
        print(data)

        room_name = data['room']
        
        room = Room.objects.filter(name=room_name)[0]

        messages = room.message_set.all().order_by('timestamp')

        content = {
            'command': 'messages',
            'messages': self.messages_to_json(messages)
        }
        self.send_chat_message(content)


    def new_message(self, data):
        
        author = data['from']
        author_user = User.objects.filter(username=author)[0]
        room = Room.objects.get(name=data['room'])

        room.time = int(data['min-timer'])
        room.save()
        
        message = Message.objects.create(
            author=author_user,
            room = room,
            content=data['message'],
            image=data['image'],
        )

        content = {
            'command': 'new_message',
            'message': self.message_to_json(message),
        }
        return self.send_chat_message(content)

    # def chat_join(self, data):

    #     print(data)
    #     author = data['from']
    #     author_user = User.objects.filter(username=author)[0]
    #     room = Room.objects.get(name=data['room'])

    #     content = {
    #         "command": "chat_join",
    #         "message": f"{author_user} just joined {room}",
    #         "from": str(author_user),
    #         "room": str(room),
    #     }
    #     return self.send_join_message(content)


    commands = {
        'fetch_messages': fetch_messages,
        'new_message': new_message,
        # 'chat_join': chat_join,
    }



    def connect(self):
        # import pdb; pdb.set_trace()

        if self.scope["user"].is_anonymous:
            #Reject connection
            self.close()

        else:

            self.room_name = self.scope['url_route']['kwargs']['room_name']
            self.room_group_name = 'chat_%s' % self.room_name

            # Join room groups
            async_to_sync(self.channel_layer.group_add)(
                self.room_group_name,
                self.channel_name
            )

            self.accept()

    def disconnect(self, close_code):
        # Leave room group
        async_to_sync(self.channel_layer.group_discard)(
            self.room_group_name,
            self.channel_name
        )

    # Receive message from WebSocket
    def receive(self, text_data):
        # import pdb; pdb.set_trace()
        print(text_data)
        data = json.loads(text_data)
        self.commands[data['command']](self, data)




    def messages_to_json(self, messages):
        result = []
        for each in messages:
            result.append(self.message_to_json(each))
        return result


    def message_to_json(self, message):
        return {
            'author': message.author.username,
            'message': message.content,
            'timestamp': str(message.timestamp),
            'image': str(message.image),
        }




    def send_chat_message(self, message):

        # Send message to room group
        async_to_sync(self.channel_layer.group_send)(
            self.room_group_name,
            {
                'type': 'chat_message',
                'message': message
            }
        )

    def send_join_message(self, message):

                # Send message to room group
        async_to_sync(self.channel_layer.group_send)(
            self.room_group_name,
            {
                'type': 'chat_join',
                'from': message['from'],
                'message': message['message'],
                'room': message['room'],
            }
        )



    def send_message(self, message):
        self.send(text_data=json.dumps(message))

    # Receive message from room group
    def chat_message(self, event):
        message = event['message']

        # Send message to WebSocket
        self.send(text_data=json.dumps(message))