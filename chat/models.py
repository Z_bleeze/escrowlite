from django.contrib.auth.models import User
from django.db import models
from core.choices import currencyChoices, paymentMethodChoices

class Room(models.Model):
    
    name = models.CharField(max_length=20)

    currency = models.CharField(max_length=100, choices=currencyChoices)
    purchase_price = models.FloatField()
    btc_price = models.FloatField()
    rate = models.FloatField()
    method = models.CharField(max_length=250, choices=paymentMethodChoices, null=True)
    terms = models.CharField(max_length=300)
    time = models.IntegerField()
    is_open = models.BooleanField(default=True)

    def __str__(self):
        return self.name


    # def get_recent_messages(self):
    #     return self.messages_set.all().order_by('-timestamp')[:10]


class Message(models.Model):
    author = models.ForeignKey(User, related_name='author_messages', on_delete=models.CASCADE)
    room = models.ForeignKey(Room, on_delete=models.CASCADE)
    content = models.TextField()
    timestamp = models.DateTimeField(auto_now_add=True)
    image = models.FileField(upload_to='media', blank=True)

    class Meta:
        ordering = ('timestamp',)

    def __str__(self):
        return self.author.username


