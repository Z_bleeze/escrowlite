from django.shortcuts import render
from django.utils.safestring import mark_safe
from django.contrib.auth.decorators import login_required
from .models import Room, Message
import json


def index(request):
    
    return render(request, "chat/index.html")


@login_required
def room(request, room_name):
    """
    Chat Room View
    """
    Room.objects.get_or_create(name=room_name)

    room = Room.objects.get(name=room_name)
    messages = room.message_set.all()

    # Sniff Out The Registered Users To Room
    active_users = []
    for msg in messages:
        user = msg.author.username       
        if user not in active_users:
            active_users.append(user)


    context = {
        'room': room,
        'users': mark_safe(active_users),
        'Usernames': active_users,
        'room_name_json': mark_safe(json.dumps(room_name)),
        'username': mark_safe(json.dumps(request.user.username)),
    }

    return render(request, 'chat/room.html', context)
