from django.contrib import admin
from .models import Account, Contact, Transaction, Card, Bank

admin.site.register(Account)
admin.site.register(Contact)
admin.site.register(Transaction)
admin.site.register(Card)
admin.site.register(Bank)