import calendar

paymentMethodChoices = (
    ("National bank transfer", "National bank transfer"),
    ("Wire Transfer", "Wire Transfer"),
    ("SEPA(EU) bank transfer", "SEPA(EU) bank transfer"),
    ("Transfers with specific bank", "Transfers with specific bank"),
    ("International Wire(SWIFT)", "International Wire(SWIFT)"),
    ("Other online payment", "Other online payment"),
    ("Swish", "Swish"),
    ("Paypal", "Paypal"),
    ("WebMoney", "WebMoney"),
    ("OKPay", "OKPay"),
    ("WebMoney", "WebMoney"),
    ("Western Union", "Western Union"),
    ("Moneygram", "Moneygram"),
    ("CashU", "CashU"),
    ("Venmo", "Venmo"),
    ("Perfect Money", "Perfect Money"),
    ("PostePay", "PostePay"),
    ("Cash at ATM", "Cash at ATM"),
    ("WeChat", "WeChat"),
    ("Walmart 2 Walmart", "Walmart 2 Walmart"),
    ("Walmart Gift Card Code", "Walmart Gift Card Code"),
    ("iTunes Gift Card Code", "iTunes Gift Card Code"),
    ("Credit Card", "Credit Card"),
    ("Worldremit", "Worldremit"),
    ("Starbucks Gift Card Code", "Starbucks Gift Card Code"),
    ("Ebay Gift Card Code", "Ebay Gift Card Code"),
    ("Yandex Money", "Yandex Money"),
    ("PayPal My Cash", "PayPal My Cash"),
    ("MobilePay", "MobilePay"),
    ("Payeer", "Payeer"),
    ("Apple Store Gift Card Code", "Apple Store Gift Card Code"),
    ("Steam Gift Card Code", "Steam Gift Card Code"),
    ("Vanilla", "Vanilla"),
    ("Google Wallet", "Google Wallet"),
    ("Chase QuickPay", "Chase QuickPay"),
    ("Venmo", "Venmo")
    )

monthChoices = (
    (calendar.month_name[i], str(i)) for i in range(1,13)
    )

dayChoices = (
    (str(i), str(i),) for i in range(1,30)
    )

currencyChoices = (
    ("USD", "USD"),
    ("CAD", "CAD"),
    ("NGN", "NGN"),
    ("JPY", "JPY"),
    ("CNY", "CNY"),
    ("SGD", "SGD"),
    ("CAD", "CAD"),
    ("HKD", "HKD"),
    ("NZD", "NZD"),
    ("AUD", "AUD"),
    ("CLP", "CLP"),
    ("GBP", "GBP"),
    ("DKK", "DKK"),
    ("SEK", "SEK"),
    ("ISK", "ISK"),
    ("CHF", "CHF"),
    ("BRL", "BRL"),
    ("EUR", "EUR"),
    ("RUB", "RUB"),
    ("PLN", "PLN"),
    ("THB", "THB"),
    ("KRW", "KRW"),
    ("TWD", "TWD"),
    )


