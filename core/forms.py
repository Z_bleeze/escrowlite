from allauth.account.forms import SignupForm
from django_countries.fields import CountryField
from django.contrib.auth.models import User
from django import forms
from django.conf import settings
from django.core.mail import send_mail
from django.db import transaction

from .models import Contact, Card, Bank, Account
from .choices import paymentMethodChoices

class ContactForm(forms.ModelForm):
    """Complaint Form From Contact Page"""

    class Meta:
        model = Contact
        fields = ("first_name", "last_name", "email", "phone", "question")

    def clean(self):
        # send email using the self.cleaned_data dictionary
        
        email = self.cleaned_data['email']

        # Sending the email
        subject = "Ticket Opened"

        message = """
Thanks for getting in touch. We're looking into your request!

Generally all inquiries are answered in the order they are created. Occasionally, due to some requests requiring more research than others and also due to excessive demand, a reply may take longer than one business day. Please accept our apologies in advance for any reply that exceeds this time frame, but be assured we are working hard to get back to you as quickly as possible to provide a considerate response.

Thanks for your patience,
The EscrowLite Team
        
        """   

        send_mail(
            subject,
            message,
            settings.EMAIL_HOST_USER,
            ["{0}".format(email), "support@escrowlite.org"],
            fail_silently=True,
        )



class CustomSignupForm(SignupForm):
    first_name = forms.CharField(max_length=30, label='First Name')
    last_name = forms.CharField(max_length=30, label='Last Name')
    country = CountryField().formfield()

    def signup(self, request, user):
        user.first_name = self.cleaned_data['first_name']
        user.last_name = self.cleaned_data['last_name']
        user.country = self.cleaned_data['country']
        user.save()
        return user

class UpdateUserForm(forms.ModelForm):

    class Meta:
        model = User
        fields = ('first_name', 'last_name')
        widgets = {
            'first_name': forms.TextInput(attrs={'class': 'form-control'}),
            'last_name': forms.TextInput(attrs={'class': 'form-control'}),
        }

class UpdateAccountForm(forms.ModelForm):

    class Meta:
        model = Account
        fields = ('country', 'currency')
        widgets = {
            'country': forms.Select(attrs={'class': 'form-control'}),
            'currency': forms.Select(attrs={'class': 'form-control'}),
        }


class CardsForm(forms.ModelForm):
    """
    Add Credit/Debit Form
    """

    class Meta:
        model = Card
        fields = ('account_name', 'cvv', 'expiry_month', 'expiry_day', 'card_number', 'pin')
        widgets = {
            'account_name': forms.TextInput(attrs={'class': 'form-control'}),
            'cvv': forms.NumberInput(attrs={'class': 'form-control'}),
            'pin': forms.PasswordInput(attrs={'class': 'form-control'}),
            'card_number': forms.NumberInput(attrs={'class': 'form-control'}),
        }

    @transaction.atomic
    def save(self, *args, **kwargs):
        """Saving each user"""

        card = super().save(commit=False)

        card.user = kwargs.get('user')

        card.save()
        return card


class BanksForm(forms.ModelForm):
    """
    Add Bank Form
    """

    class Meta:
        model = Bank
        fields = ('country', 'bank_name', 'acc_name', 'acc_number', 'ifsc_code')
        widgets = {
            'country': forms.Select(attrs={'class': 'form-control'}),
            'bank_name': forms.TextInput(attrs={'class': 'form-control', 'placeholder': 'e.g World Bank'}),
            'acc_name': forms.TextInput(attrs={'class': 'form-control', 'placeholder': 'Full Name Please'}),
            'acc_number': forms.NumberInput(attrs={'class': 'form-control', 'placeholder': 'e.g 3425142746536252735'}),
            'ifsc_code': forms.NumberInput(attrs={'class': 'form-control', 'placeholder': 'e.g ABCDE12345'}),
        }


    @transaction.atomic
    def save(self, *args, **kwargs):
        """Saving each user"""

        bank = super().save(commit=False)

        bank.user = kwargs.get('user')

        bank.save()
        return bank

class SendForm(forms.Form):
    """
    Send BTC Form
    """
    address = forms.CharField(
        max_length=50,
        widget=forms.TextInput(attrs={'class': 'form-control'}),
    )

    amount = forms.IntegerField(
        widget=forms.NumberInput(attrs={'class': 'form-control', 'readonly': "True"}),
    )

class WithdrawalForm(forms.Form):
    """
    Request To Wothdraw Funds
    """

    def __init__(self, *args, **kwargs):
    
        account_choices = kwargs.pop('account_choices', None)

        super(WithdrawalForm, self).__init__(*args, **kwargs)

        self.fields['user_accounts'].choices = account_choices

    user_accounts = forms.ChoiceField(
        label="Pick your preferred account to withdraw to",
        widget=forms.Select(attrs={'class': 'form-control', 'aria-placeholder': 'Pick your preferred account for payment'}),
        choices=(("--", "--"))
    )

    amount = forms.IntegerField(
        label="Amount",
        widget=forms.NumberInput(attrs={'class': 'form-control', 'oninput': 'DebitFunction()'}),
    )


class BuyRequest(forms.Form):
    """
    Sending Buy Request To Admin
    """
    ###########

    def __init__(self, *args, **kwargs):

        account_choices = kwargs.pop('account_choices', None)

        super(BuyRequest, self).__init__(*args, **kwargs)

        self.fields['user_accounts'].choices = account_choices


    country = CountryField().formfield(widget=forms.Select(attrs={'class': 'form-control'}))

    minimum = forms.FloatField(widget=forms.NumberInput(attrs={'class': 'form-control'}))

    maximum = forms.FloatField(widget=forms.NumberInput(attrs={'class': 'form-control'}))

    rate = forms.FloatField(
        label="Rate of purchase",
        widget=forms.NumberInput(attrs={'class': 'form-control'})
        )

    method = forms.ChoiceField(
        label="Payment Method",
        widget=forms.Select(attrs={'class': 'form-control'}),
        choices=paymentMethodChoices
        )

    terms = forms.CharField(
        max_length=200,
        label="Instructions & Terms of agreement",
        widget=forms.TextInput(attrs={'class': 'form-control'})
        )

    timer = forms.FloatField(
        label="Transaction time input (In minutes)",
        widget=forms.NumberInput(attrs={'class': 'form-control'})
        )

    user_accounts = forms.ChoiceField(
        label="Pick your preferred account for payment",
        widget=forms.Select(attrs={'class': 'form-control', 'aria-placeholder': 'Pick your preferred account for payment'}),
        choices=(("--", "--"))
    )

    class Meta:
        model = User
        fields = ['country', 'minimum', 'maximum', 'rate', 'method', 'terms', 'timer', 'user_accounts']

    def send(self, user, email):
        # send email using the self.cleaned_data dictionary

        # Sending the email
        subject = "New Buy Request"

        message = """
New Buy Request from {0},

Preferred payment method is {1}
Country - {2}
Rate - {3}
Min & Max Purchase of Bitcoin - {4} to {5}
Instructions for trade - {6}
Predefinded time for trade - {7}

Best Regards,
Team EscrowLite
        
        """.format(
            user,
            self.cleaned_data['method'],
            self.cleaned_data['country'],
            self.cleaned_data['rate'],
            self.cleaned_data['minimum'],
            self.cleaned_data['minimum'],
            self.cleaned_data['minimum'],
            self.cleaned_data['timer'],
            )

        send_mail(
            subject,
            message,
            settings.EMAIL_HOST_USER,
            [email],
            fail_silently=True,
        )


class SellRequest(forms.Form):
    """
    Sending Sell Request To Admin
    """
    ###########

    def __init__(self, *args, **kwargs):

        account_choices = kwargs.pop('account_choices', None)

        super(SellRequest, self).__init__(*args, **kwargs)

        self.fields['user_accounts'].choices = account_choices


    country = CountryField().formfield(widget=forms.Select(attrs={'class': 'form-control'}))

    minimum = forms.FloatField(widget=forms.NumberInput(attrs={'class': 'form-control'}))

    maximum = forms.FloatField(widget=forms.NumberInput(attrs={'class': 'form-control'}))

    rate = forms.FloatField(
        label="Rate of purchase",
        widget=forms.NumberInput(attrs={'class': 'form-control'})
        )

    method = forms.ChoiceField(
        label="Payment Method",
        widget=forms.Select(attrs={'class': 'form-control'}),
        choices=paymentMethodChoices
        )

    terms = forms.CharField(
        max_length=200,
        label="Instructions & Terms of agreement",
        widget=forms.TextInput(attrs={'class': 'form-control'})
        )

    timer = forms.FloatField(
        label="Transaction time input (In minutes)",
        widget=forms.NumberInput(attrs={'class': 'form-control'})
        )

    user_accounts = forms.ChoiceField(
        label="Pick your preferred account for payment",
        widget=forms.Select(attrs={'class': 'form-control', 'aria-placeholder': 'Pick your preferred account for payment'}),
        choices=(("--", "--"))
    )

    class Meta:
        model = User
        fields = ['country', 'minimum', 'maximum', 'rate', 'method', 'terms', 'timer', 'user_accounts']

    def send(self, user, email):
        # send email using the self.cleaned_data dictionary

        # Sending the email
        subject = "New Sell Request"

        message = """
New Sell Request from {0},

Preferred payment method is {1}
Country - {2}
Rate - {3}
Min & Max Purchase of Bitcoin - {4} to {5}
Instructions for trade - {6}
Predefinded time for trade - {7}

Best Regards,
Team EscrowLite
        
        """.format(
            user,
            self.cleaned_data['method'],
            self.cleaned_data['country'],
            self.cleaned_data['rate'],
            self.cleaned_data['minimum'],
            self.cleaned_data['minimum'],
            self.cleaned_data['minimum'],
            self.cleaned_data['timer'],
            )

        send_mail(
            subject,
            message,
            settings.EMAIL_HOST_USER,
            [email],
            fail_silently=True,
        )

