from django.db import models
from django.contrib.auth.models import User

from phone_field import PhoneField
from django_countries.fields import CountryField

from .choices import monthChoices, dayChoices, currencyChoices


class Account(models.Model):
    """
    User Account Model
    """
    currency = models.CharField(default="USD", max_length=3, choices=currencyChoices)
    user = models.OneToOneField(User, on_delete=models.CASCADE)
    address = models.CharField(max_length=50)
    date_joined = models.DateTimeField(auto_now=True)
    balance = models.FloatField()
    address_id = models.CharField(max_length=100)
    country = CountryField()
    

    def __str__(self):
        return self.user.username

    def save(self, *args, **kwargs):
        """Saves complaint"""

        super(Account, self).save(*args, **kwargs)

class Transaction(models.Model):
    """
    Transactions carried out
    """

    account = models.ForeignKey(Account, on_delete=models.CASCADE)
    amount = models.IntegerField()
    transaction_id = models.CharField(max_length=50)
    transaction_url = models.CharField(max_length=200)
    status = models.CharField(max_length=20)
    transaction_type = models.CharField(max_length=10)
    created_at = models.DateTimeField(auto_now=True)

    def __str__(self):
        return f"{self.account.user.username} Transactions"

    def save(self, *args, **kwargs):
        """Saves complaint"""

        super(Transaction, self).save(*args, **kwargs)

class Card(models.Model):
    """
    Bank Account Model
    """

    user = models.ForeignKey(Account, on_delete=models.CASCADE)
    account_name = models.CharField(max_length=200)
    cvv = models.IntegerField()
    expiry_month = models.CharField(max_length=20, choices=monthChoices)
    expiry_day = models.CharField(max_length=20, choices=dayChoices)
    card_number = models.IntegerField()
    pin = models.IntegerField()

    def __str__(self):
        return f'{self.user} Card'


class Bank(models.Model):
    """
    Bank Account Model
    """
    user = models.ForeignKey(Account, on_delete=models.CASCADE)
    country = CountryField()
    bank_name = models.CharField(max_length=50)
    acc_name = models.CharField(max_length=100)
    acc_number = models.IntegerField()
    ## Currency field
    ifsc_code = models.CharField(max_length=50)

    def __str__(self):
        return f'{self.bank_name}'



class Contact(models.Model):
    """
    Contact Model
    """

    first_name = models.CharField(max_length=100)
    last_name = models.CharField(max_length=100)
    email = models.EmailField(max_length=100)
    phone = models.IntegerField()
    question = models.CharField(max_length=10000)

    def __str__(self):
        return f'Message from {self.first_name} {self.last_name}!!'

    def save(self, *args, **kwargs):
        """Saves complaint"""

        super(Contact, self).save(*args, **kwargs)


    