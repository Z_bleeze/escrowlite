from django.urls import path, include, re_path
from .views import index, About, Contact, Terms, FAQ, Receive, Send, Banks, NewCard, NewBank, Transactions, profile, buy_request, Withdraw, sell_request, Notifications, delete_notification, detail_notification, delete_card, delete_bank

app_name = 'core'

urlpatterns = [

    path('', index, name='index'),
    path('about/', About.as_view(), name='about'),
    path('contact/', Contact.as_view(), name='contact'),
    path('terms/', Terms.as_view(), name='terms'),
    path('faq/', FAQ.as_view(), name='faq'),

    # User Profile Settings
    path('settings/', profile, name='settings'),

    # Send & Receive
    path('receive/', Receive, name='receive'),
    path('send/', Send, name='send'),
    path('transactions/', Transactions, name='transactions'),
    path('withdraw/', Withdraw, name='withdraw'),

    # Notification Session
    path('notifications/', Notifications, name='notifications'),
    re_path('^delete/(?P<pk>[0-9]+)/$', delete_notification, name='delete_notification'),
    re_path('^detail/(?P<pk>[0-9]+)/$', detail_notification, name='detail_notification'),

    # Bank Accounts
    path('linked_accounts/', Banks, name='cards&banks'),
	path('add_card/', NewCard, name='add_card'),
    re_path('^delete_card/(?P<pk>[0-9]+)/$', delete_card, name='delete_card'),
	path('add_bank/', NewBank.as_view(), name='add_bank'),
    re_path('^delete_bank/(?P<pk>[0-9]+)/$', delete_bank, name='delete_bank'),

    # Buy Bitcoin Request
    path('buy_request/', buy_request, name="buy_request"),
    path('sell_request/', sell_request, name="sell_request"),
]