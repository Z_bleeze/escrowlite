from django.shortcuts import render, redirect, get_object_or_404
from django.contrib.auth.models import User
from django.contrib.auth.decorators import login_required
from django.db.models.signals import post_save
from django.contrib import messages
from django.core.mail import send_mail
from django.conf import settings
from django.views.generic import CreateView, TemplateView, UpdateView, DetailView
from django.views import View
from django.views.generic.edit import FormView
from django.conf import settings

from .forms import ContactForm, CardsForm, BanksForm, CustomSignupForm, UpdateUserForm, UpdateAccountForm, BuyRequest, SendForm, WithdrawalForm, SellRequest
from .models import Contact, Card, Bank, Transaction, Account

from trade.models import Buyer, Seller

from notifications.signals import notify
from coinbase.wallet.client import Client
import json
from random import shuffle
import cryptocompare

# SETTING UP COINBASE CLIENT
client = Client(api_key=settings.API_KEY, api_secret=settings.API_SECRET)


def get_price(currency_code):
    """
    Returning the current btc price for specified currency
    """
    data = cryptocompare.get_price('BTC', curr=currency_code)
    return data['BTC'][currency_code]


def index(request):
    """Base Index Page"""

    template_name = 'core/index.html'

    buyers = Buyer.objects.all()
    shuffle(buyers)

    sellers = Seller.objects.all()
    shuffle(sellers)

    if request.user.is_authenticated == True:
        account = client.get_primary_account()
        
        try:
            person = Account.objects.get(user=request.user)

            wallet_balance = person.balance
            balance = "%.5f" % wallet_balance

            # Getting the present bitcoin value in that currency
            currency = person.currency
            price = get_price(currency)

        except Account.DoesNotExist:

            # Creating the wallet
            address = client.create_address(account.id)

            wallet_balance = 0.000
            balance = "%.5f" % wallet_balance

            Account.objects.create(
                user = request.user,
                address = address['address'],
                address_id = address['id'],
                balance = wallet_balance,
                )

            currency = "USD"
            price = get_price(currency_code=currency)
        
        currency_balance = float(price) * float(balance)
        currency_balance = "%.2f" % currency_balance
        
        context = {
            'buyers': buyers[:4],
            'seller': sellers[:4],
            'currency_balance': currency_balance,
            'currency': currency,
            'price': price,
            'balance': balance,
        }

        return render(request, template_name, context)

    context = {
        'buyers': buyers[:4],
        'sellers': sellers[:4],
    }


    return render(request, template_name, context)


class Terms(TemplateView):
    """Terms And Conditions"""
    template_name = 'core/terms.html'


class About(TemplateView):
    """About Page"""
    template_name = 'core/about.html'

class FAQ(TemplateView):
    """FAQ Page"""
    template_name = 'core/faq.html'


class Contact(CreateView):
    """Contact Us Page"""

    model = Contact
    form_class = ContactForm
    template_name = 'core/contact.html'

    def post(self, request, *args, **kwargs):

        form = self.form_class(request.POST)

        if form.is_valid():

            contact = form.save()
                
            contact.save()
            contact.clean()

            messages.success(self.request, "Message Sent")

            return redirect('core:index')



@login_required
def profile(request):
    """Profile Update Page"""
    
    template = "core/dashboard.html"
    
    if request.method == "GET":

        form1 = UpdateUserForm(instance=request.user)

        form2 = UpdateAccountForm(instance=request.user.account)

        context = {
            'form1': form1,
            'form2': form2,
        }

        return render(request, template, context)

    elif request.method == "POST":

        form1 = UpdateUserForm(request.POST, instance=request.user)
        form1.save()
        form2 = UpdateAccountForm(request.POST, instance=request.user.account)
        form2.save()

        sender = User.objects.first()
        # Notification object
        notify.send(
            sender,
            recipient=request.user,
            verb="Profile Updated!",
            description="Your account profile information was just updated",
            )

        messages.success(request, "Account updated!")
        return redirect('core:index')

@login_required
def Receive(request):
    """
    Receive Bitcoin
    """
    account = Account.objects.get(user=request.user)
    address = account.address
    balance = account.balance

    # Getting the present bitcoin value in that currency
    currency = account.currency
    price = get_price(currency_code=currency)

    currency_balance = float(price) * float(balance)
    currency_balance = "%.2f" % currency_balance

    context = {
        'currency_balance': currency_balance,
        'price': price,
        'currency': currency,
        'address' : address,
        'balance' : "%.5f" % balance
    }
    return render(request, "core/receive.html", context)

@login_required
def Send(request):
    """
    Send Bitcion
    """
    template_name = "core/send.html"

    balance = Account.objects.get(user=request.user).balance

    # Getting the present bitcoin value in that currency
    currency = Account.objects.get(user=request.user).currency
    price = get_price(currency_code=currency)

    currency_balance = float(price) * float(balance)
    currency_balance = "%.2f" % currency_balance

    if request.method == "POST":

        form = SendForm(request.POST)
        
        if float(form.data['amount']) > float(balance):
            messages.info(request, "Insufficient Balance!")
            return redirect("core:send")


        sender = User.objects.first()
        # Notification object
        notify.send(
            sender,
            recipient=request.user,
            verb="Payout Request Sent!",
            description="We encountered an error while trying to send out {0} to {1}. Please try again in a few minutes and if you still face the same problems, contact support.".format(form.data['amount'], form.data['adress']),
            )

        messages.success(request, "There is an issue sending funds out! Please contact support")
        return redirect("core:index")

    form = SendForm()

    context = {
        'form': form,
        'currency_balance': currency_balance,
        'price': price,
        'currency': currency,
        'balance': "%.5f" % balance,
    }
    return render(request, template_name, context)


@login_required
def Withdraw(request):
    """
    Withdrawal Page
    """
    template_name = "core/withdraw.html"
    
    balance = Account.objects.get(user=request.user).balance

    # Getting the present bitcoin value in that currency
    currency = Account.objects.get(user=request.user).currency
    price = get_price(currency_code=currency)

    currency_balance = float(price) * float(balance)
    currency_balance = "%.2f" % currency_balance

    accounts = Bank.objects.filter(user=request.user.account)

    choices = zip(accounts, accounts)

    if request.method == "POST":
        form = WithdrawalForm(request.POST, account_choices=choices)

        if form.is_valid():

            if float(form.data['amount']) >= float(currency_balance):
                messages.info(request, "Insufficient Balance!")
                return redirect("core:withdraw")

            sender = User.objects.first()
            # Notification object
            notify.send(
                sender,
                recipient=request.user,
                verb="Withdraw Funds",
                description="There is a error in the processing of this request. Please try again!!",
                )

            messages.success(request, "Withdrawal Request Sent!")
            return redirect("core:index")
    
    else:
        form = WithdrawalForm(account_choices=choices)

    context = {
        'form': form,
        'currency_balance': currency_balance,
        'price': price,
        'currency': currency,
        'balance': "%.5f" % balance,
    }

    return render(request, template_name, context)


@login_required
def Banks(request):
    """
    Linked Banks
    """
    template_name = "core/card&bank.html"

    banks = Bank.objects.filter(user=request.user.account)
    cards = Card.objects.filter(user=request.user.account)

    balance = Account.objects.get(user=request.user).balance

    # Getting the present bitcoin value in that currency
    currency = Account.objects.get(user=request.user).currency
    price = get_price(currency)

    currency_balance = float(price) * float(balance)
    currency_balance = "%.2f" % currency_balance

    context = {
        'currency_balance': currency_balance,
        'price': price,
        'currency': currency,
        'balance': "%.5f" % balance,
        'cards': cards,
        'banks': banks,
    }

    return render(request, template_name, context)

@login_required
def NewCard(request):
    """
    Adding New Card
    """

    model = Card
    template_name = "core/add_card.html"

    if request.method == "POST":
        form = CardsForm(request.POST)
        
        if form.is_valid():

            form.save(user=request.user.account)

            sender = User.objects.first()
            # Notification object
            notify.send(
                sender,
                recipient=request.user,
                verb="New Card Added!",
                description="A new bank card has been attached to this account for payment purpose. Visit CARDS&BANKS page to get full information.",
                )

            messages.success(request, "New Card Added!")

            return redirect('core:cards&banks')
    
    else:
        form = CardsForm()
        return render(request, "core/add_card.html", {'form': form})

@login_required
def delete_card(request, pk):
    "Delete Card"   
    card = Card.objects.get(id=pk)
    card.delete()
    messages.success(request, "Card Deleted!")
    return redirect('core:cards&banks')


class NewBank(CreateView):
    """
    Adding New Bank Account
    """
    form_class = BanksForm
    model = Bank
    template_name = "core/add_bank.html"

    @login_required
    def post(self, request, *args, **kwargs):

        form = self.form_class(request.POST)
        form.user = self.request.user

        if form.is_valid():  
            form.save(user=request.user.account)
            
            # Notification object
            notify.send(
                sender=User.objects.first(),
                recipient=request.user,
                verb="New Bank Account Added!",
                description="A new bank account has been attached to this account for payment and wuthdrawal purposes. Visit CARDS&BANKS page to get full information.",
                )

            messages.success(self.request, "New Bank Account Added!")
            return redirect('core:cards&banks')

        return render(request, template_name, {'form': form})

@login_required
def delete_bank(request, pk):
    "Delete Card"   
    bank = Bank.objects.get(id=pk)
    bank.delete()
    messages.success(request, "Bank Account Deleted!")
    return redirect('core:cards&banks')

@login_required
def Transactions(request):
    """Transaction Page"""

    person = Account.objects.get(user=request.user)

    account = client.get_primary_account()

    # Analyize transactions
    transactions = account.get_address_transactions(person.address_id).data

    # Updating user balance
    balance_list = [
        float(i['amount']['amount']) for i in transactions
    ]
    if balance_list == []:
        pass
    if len(balance_list) == 1:
        person.balance += balance_list[0]
        person.save()
    else:
        person.balance += float(sum(map(float, balance_list)))
        person.save()


    for each in transactions:
        try:
            Transaction.objects.get(transaction_id=each['id'])

        except Transaction.DoesNotExist:

            # Create Transactions Information
            Transaction.objects.create(
                account = person,
                amount = each['amount'],
                transaction_id = each['id'],
                transaction_url = each['network']['url'],
                status = each['status'],
                transaction_type = each['type'],
                created_at = each['created_at'],
                )


    try:
        record = Transaction.objects.filter(account=request.user.account)

        return render(request, "core/transaction.html", {'record': record})

    except Transaction.DoesNotExist:

        return render(request, "core/transaction.html")


@login_required
def Notifications(request):
    """
    Notification Page
    """
    template = "core/notification.html"

    user = request.user
    # import pdb; pdb.set_trace()

    data = user.notifications.all()
    context = {
        'data': data
    }

    return render(request, template, context)

@login_required
def delete_notification(request, pk):
    "Delete Notification"
    notification = request.user.notifications.get(id=pk)
    notification.delete()
    return redirect('core:notifications')

@login_required
def detail_notification(request, pk):
    "Detailed Notification"
    notification = request.user.notifications.get(id=pk)
    notification.mark_as_read()
    return render(request, 'core/detail_notification.html', {'data' : notification})





@login_required
def buy_request(request):
    """
    Sending a Buy Request
    """

    user = request.user.username
    balance = Account.objects.get(user=request.user).balance

    template_name = "core/buy_request.html"

    accounts = Bank.objects.filter(user=request.user.account)

    choices = [("None", "None of the following")] + list(zip(accounts, accounts))

    tuple(choices)
    if request.method == "POST":
        form = BuyRequest(request.POST, account_choices=choices)
        
        email = request.user.email

        if form.is_valid():

            # Sending The Email
            # form.send(user=user, email=email)

            # Notification object
            notify.send(
                sender=User.objects.first(),
                recipient=request.user,
                verb="Sent New Buy Request!",
                description="""
    You just created a new buy request and your tarde details have been updated to the search list.
    
    More information considering trade requests will be forwared to your email.

    Thank you for using Escrow Lite.
                """,
                )


            # Create Buy Request
            Buyer.objects.create(
                user = request.user.account,
                name = request.user.username,
                currency = request.user.account.currency,
                min_btc = form.data['minimum'],
                max_btc = form.data['maximum'],
                rate = form.data['rate'],
                method = form.data['method'],
                terms = form.data['terms'],
                timer = form.data['timer'],
            )

            messages.success(request, "Buy Request Created!")

            return redirect('core:index')

    else:

        form = BuyRequest(account_choices=choices)

    # Getting the present bitcoin value in that currency
    currency = Account.objects.get(user=request.user).currency
    price = get_price(currency)

    currency_balance = float(price) * float(balance)
    currency_balance = "%.2f" % currency_balance

    context = {
        'currency_balance': currency_balance,
        'price': price,
        'currency': currency,
        'form': form,
        'balance': balance,
    }
    
    return render(request, template_name, context)


@login_required
def sell_request(request):

    user = request.user.username

    balance = Account.objects.get(user=request.user).balance

    template_name = "core/sell_request.html"

    accounts = Bank.objects.filter(user=request.user.account)

    choices = [("None", "None of the following")] + list(zip(accounts, accounts))

    tuple(choices)
    if request.method == "POST":
        form = SellRequest(request.POST, account_choices=choices)
        
        email = request.user.email

        if form.is_valid():

            # Sending The Email
            # form.send(user=user, email=email)

            # Notification object
            notify.send(
                sender=User.objects.first(),
                recipient=request.user,
                verb="Sent New Sell Request!",
                description="""
    You just created a new sell request and your tarde details have been updated to the search list.
    
    More information considering trade requests will be forwared to your email.

    Thank you for using Escrow Lite.
                """,
                )


            # Create Sell Request
            Seller.objects.create(
                user = request.user.account,
                name = request.user.username,
                currency = request.user.account.currency,
                min_btc = form.data['minimum'],
                max_btc = form.data['maximum'],
                rate = form.data['rate'],
                method = form.data['method'],
                terms = form.data['terms'],
                timer = form.data['timer'],
            )

            messages.success(request, "Sell Request Created!")

            return redirect('core:index')

    else:

        form = SellRequest(account_choices=choices)

    # Getting the present bitcoin value in that currency
    currency = Account.objects.get(user=request.user).currency
    price = get_price(currency)

    currency_balance = float(price) * float(balance)
    currency_balance = "%.2f" % currency_balance

    context = {
        'currency_balance': currency_balance,
        'price': price,
        'currency': currency,
        'form': form,
        'balance': balance,
    }
    
    return render(request, template_name, context)


