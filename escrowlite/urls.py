
from django.contrib import admin
from django.urls import path, include
import notifications.urls

urlpatterns = [
    path('admin/', admin.site.urls),
    path('', include('core.urls')),
    path('accounts/', include('allauth.urls')),
    path('chat/', include('chat.urls')),
    path('trade/', include('trade.urls')),
    path('notifications/', include(notifications.urls, namespace='notifications')),
]
