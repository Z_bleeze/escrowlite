from django import forms
from .models import Buyer, Seller
from django.contrib.auth.models import User

from django.conf import settings
from django.core.mail import send_mail

from chat.models import Room

class TradeForm(forms.Form):
    """
    Form For Opening Trade Request
    """

    amount = forms.FloatField(
        widget=forms.NumberInput(attrs={'class': 'form-row', 'oninput': 'CalculateByRateFromBtc()'}),
    )

    currency_value = forms.FloatField(
        widget=forms.NumberInput(attrs={'class': 'form-row', 'oninput': 'CalculateByRateFromCurrency()'})
    )


    def send(self, user, email, info):
        """
        Send Request To Trader
        """

        # Sending the email
        subject = f"Request From {user}"

        message = """
You Have A New Request from {0},

Your Request:
Bitcoin you wish to buy is {8}
The present value in {2} is {9}

Trader Dashboard Information:

Preferred payment method is {1}
User currency - {2}
Rate - {3}
Min & Max Purchase of Bitcoin - {4} to {5}
Instructions for trade - {6}
Predefinded time for trade - {7}

Best Regards,
Team EscrowLite
        
        """.format(
            user,
            info.method,
            info.currency,
            info.rate,
            info.min_btc,
            info.max_btc,
            info.terms,
            info.timer,
            self.cleaned_data['amount'],
            self.cleaned_data['currency_value'],
            )

        send_mail(
            subject,
            message,
            settings.EMAIL_HOST_USER,
            [email],
            fail_silently=True,
        )



class OpenTradeForm(forms.ModelForm):
    """
    Creating A New Websocket Room
    """

    def __init__(self, *args, **kwargs):
        
        request_choices = kwargs.pop('requests', None)
        user_choices = kwargs.pop('users', None)

        super(OpenTradeForm, self).__init__(*args, **kwargs)

        self.fields['trade_id'].choices = request_choices
        self.fields['recipient_name'].choices = user_choices

    recipient_name = forms.ChoiceField(
        widget=forms.Select(attrs={'class': 'form-control'}),
        choices=(("--", "--")),
    )

    trade_id = forms.ChoiceField(
        widget=forms.Select(attrs={'class': 'form-control'}),
        choices=(("--", "--")),
    )

    class Meta:
        model = Room
        fields = ['purchase_price', 'btc_price']
        widgets = {
            'purchase_price': forms.NumberInput(attrs={'class': 'form-control'}),
            'btc_price': forms.NumberInput(attrs={'class': 'form-control'}),
        }

    def send(self, user, name, email):
        """
        Send Tade Link To User
        """

        # Sending the email
        subject = f"New Trade Initiated With {user}"

        message = f"""
A new trade window has been created for you and {user} on EscrowLite secured escrow service.

www.escrowlite/chat/{name}



Log into your escrowlite account to see more information regarding this trade - www.escrowlite.org

Happy Trading!


Best Regards,
Team EscrowLite
        
        """

        send_mail(
            subject,
            message,
            settings.EMAIL_HOST_USER,
            [email, user.email],
            fail_silently=True,
        )