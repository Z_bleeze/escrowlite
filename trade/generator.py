import random

import string

def passkey():
    "Generates a password"

    pin = ""

    lower_case = string.ascii_lowercase

    upper_case = string.ascii_uppercase

    # symbols = string.punctuation

    
    length = random.randint(10,15)
    

    for i in range(length):

        choice = random.randint(0,2)

        if choice == 0:

            random_lower = random.choice(lower_case)
            pin += random_lower


        if choice == 1:

            random_upper = random.choice(upper_case)
            pin += random_upper
    

        if choice == 2:

            random_num = random.randint(0, 9)
            pin += str(random_num)

    return pin

