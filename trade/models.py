from django.db import models
from django_countries.fields import CountryField
from core.choices import currencyChoices, paymentMethodChoices
from core.models import Account

class Seller(models.Model):
    """
    Sellers List
    """

    user = models.ForeignKey(Account, on_delete=models.CASCADE)
    name = models.CharField(max_length=50)
    currency = models.CharField(max_length=100, choices=currencyChoices)
    min_btc = models.FloatField()
    max_btc = models.FloatField()
    rate = models.FloatField()
    method = models.CharField(max_length=250, choices=paymentMethodChoices)
    terms = models.CharField(max_length=300)
    timer = models.IntegerField()
    is_online = models.BooleanField(default=False)

    def __str__(self):
        return f"{self.user} as {self.name} - {self.rate}/{self.currency}"


class Buyer(models.Model):
    """
    Buyers List
    """

    user = models.ForeignKey(Account, on_delete=models.CASCADE)
    name = models.CharField(max_length=50)
    currency = models.CharField(max_length=100, choices=currencyChoices)
    min_btc = models.FloatField()
    max_btc = models.FloatField()
    rate = models.FloatField()
    method = models.CharField(max_length=250, choices=paymentMethodChoices)
    terms = models.CharField(max_length=300, null=True, blank=True)
    timer = models.IntegerField()
    is_online = models.BooleanField(default=False)

    def __str__(self):
        return f"{self.user} as {self.name} - {self.rate}/{self.currency}"