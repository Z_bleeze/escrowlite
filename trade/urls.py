from django.urls import path
from .views import sellers, buyers, top_sellers, top_buyers, seller_detail, buyer_detail, open_trade

app_name = 'trade'

urlpatterns = [
    path("quick_sell/", sellers, name="sell"),
    path("quick_buy/", buyers, name="buy"),

    # Previewed Top Sellers And Buyers
    path('top_sellers/', top_sellers, name="top_sell"),
    path('top_buyers/', top_buyers, name="top_buy"),

    path('sellers/<int:pk>/', seller_detail, name="seller_detail"),
    path('buyers/<int:pk>/', buyer_detail, name="buyer_detail"),

    path('open_trade/', open_trade, name="open_trade"),
]
