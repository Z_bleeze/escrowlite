from django.shortcuts import render, redirect
from django.views.generic import DetailView, FormView, UpdateView
from django.contrib import messages
from django.contrib.auth.models import User

from .models import Seller, Buyer
from .forms import TradeForm, OpenTradeForm
from .generator import passkey

from chat.models import Room
from notifications.signals import notify
from random import shuffle

def sellers(request):
    """
    Sellers List Page
    """

    template = "trade/sell.html"

    currency = request.user.account.currency

    data = Seller.objects.filter(currency=currency)

    context = {
        'currency': currency,
        'data': data
    }

    return render(request, template, context)


def buyers(request):
    """
    Sellers List Page
    """

    template = "trade/buy.html"

    currency = request.user.account.currency

    data = Buyer.objects.filter(currency=currency)

    context = {
        'currency': currency,
        'data': data
    }

    return render(request, template, context)


def top_sellers(request):
    """
    List of Sellers
    """

    template = "trade/top_sell.html"

    sellers = Seller.objects.all()

    shuffle(sellers)
    context = {
        'data': sellers
    }

    return render(request, template, context)

def top_buyers(request):
    """
    List of buyers
    """

    template = "trade/top_buy.html"

    buyers = Buyer.objects.all()

    shuffle(buyers)
    context = {
        'data': buyers
    }

    return render(request, template, context)


def seller_detail(request, pk):
    """
    Displaying Seller Object
    """

    object = Seller.objects.get(pk=pk)

    if request.method == "POST":
        form = TradeForm(request.POST)

        if form.is_valid():

            form.send(user=request.user, email=object.user.user.email, info=object)

            # Notify Buyer
            notify.send(
                sender=request.user,
                recipient=object.user.user,
                verb=f"Buy Request From {request.user}",
                description="You just received a trade request from buyer. Please check your email to get information of trade."
            )

            # Notification object
            notify.send(
                sender = User.objects.first(),
                recipient = request.user,
                verb = f"You Sent A Buy Request To {object.user.user}",
                description = "Your request to buy {0} bitcoin at {1}{2}/BTC has been sent successfully. Please check your email and await further instructions on trade.".format(form.data['amount'],object.rate,object.currency),
                )

            messages.success(request, "Request Sent!")
            return redirect('core:index')
    else:
        form = TradeForm()

    context = {
        'object': object,
        'form' : form
    }

    return render(request, "trade/seller.html", context)

def buyer_detail(request, pk):
    """
    Displaying Buyer Object
    """

    object = Buyer.objects.get(pk=pk)
    form = TradeForm()

    if request.method == "POST":
        form = TradeForm(request.POST)

        if form.is_valid():

            form.send(user=request.user, email=object.user.user.email, info=object)

            # Notify Buyer
            notify.send(
                sender=request.user,
                recipient=object.user.user,
                verb=f"Sell Request From {request.user}",
                description="You just received a trade request from seller. Please check your email to get information of trade."
            )

            # Notification object
            notify.send(
                sender = User.objects.first(),
                recipient = request.user,
                verb = f"You Sent A Sell Request To {object.user.user}",
                description = "Your request to sell {0} bitcoin at {1}{2}/BTC has been sent successfully. Please check your email and await further instructions on trade.".format(form.data['amount'],object.rate,object.currency),
                )

            messages.success(request, "Request Sent!")
            return redirect('core:index')
    else:
        form = TradeForm()

    context = {
        'object': object,
        'form' : form
    }

    return render(request, "trade/buyer.html", context)


def open_trade(request):
    """
    Function To Create A New Room And Notify The Users Under It
    """

    # buy_requests = Buyer.objects.filter(name=request.user.username)
    sellersList = Seller.objects.all()
    sellers = [i.pk for i in sellersList]

    usersList = User.objects.all()
    users = [i.username for i in usersList]

    users = zip(users, users)
    requests = zip(sellers, sellers)

    if request.method == "POST":

        form = OpenTradeForm(request.POST, requests=requests, users=users)
        
        if form.is_valid():
            import pdb; pdb.set_trace()
            recipient = form.data['recipient_name']
            trade_id = form.data['trade_id']

            user = User.objects.get(username=recipient)
            sell_request = Seller.objects.get(pk=int(trade_id))
            
            name = str(passkey())

            form.send(user=request.user, name=name, email=user.email)

            chat = Room.objects.create(
                name = name,
                currency = sell_request.currency,
                purchase_price = form.data['purchase_price'],
                btc_price =  form.data['btc_price'],
                rate = sell_request.rate,
                method = sell_request.method,
                terms = sell_request.terms,
                time = sell_request.timer,
            )
            chat.save()

            # Notify Buyer
            notify.send(
                sender=request.user,
                recipient=user,
                verb=f"New Trade Started!",
                description=f"""
                    Your trade request was accepted by {request.user.username}, 
                    
                Open the live chat right now at
                
                www.escrowlite.org/chat/{name}/
                    """,
            )

            # Notification object
            notify.send(
                sender = User.objects.first(),
                recipient = request.user,
                verb = f"New Trade Started!",
                description = f"""
                    You just opened a new trade window at
                    
                    www.escrowlite.org/chat/{name}/
                    """,
                )

            messages.success(request, "Request Sent!")
            return redirect('core:index')


    form = OpenTradeForm(requests=requests, users=users)

    return render(request, "trade/create.html", {'form': form})